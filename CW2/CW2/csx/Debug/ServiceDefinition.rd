﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="CW2" generation="1" functional="0" release="0" Id="b9f45d84-38d1-45d6-9340-efc2b3018425" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="CW2Group" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="CloudletService:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/CW2/CW2Group/LB:CloudletService:Endpoint1" />
          </inToChannel>
        </inPort>
        <inPort name="Engine:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/CW2/CW2Group/LB:Engine:Endpoint1" />
          </inToChannel>
        </inPort>
        <inPort name="WebInterface:Endpoint1" protocol="https">
          <inToChannel>
            <lBChannelMoniker name="/CW2/CW2Group/LB:WebInterface:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="Certificate|WebInterface:Certificate1" defaultValue="">
          <maps>
            <mapMoniker name="/CW2/CW2Group/MapCertificate|WebInterface:Certificate1" />
          </maps>
        </aCS>
        <aCS name="CloudletService:Blobs" defaultValue="">
          <maps>
            <mapMoniker name="/CW2/CW2Group/MapCloudletService:Blobs" />
          </maps>
        </aCS>
        <aCS name="CloudletService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/CW2/CW2Group/MapCloudletService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="CloudletServiceInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/CW2/CW2Group/MapCloudletServiceInstances" />
          </maps>
        </aCS>
        <aCS name="Engine:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/CW2/CW2Group/MapEngine:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="EngineInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/CW2/CW2Group/MapEngineInstances" />
          </maps>
        </aCS>
        <aCS name="WebInterface:ConnectToBlobs" defaultValue="">
          <maps>
            <mapMoniker name="/CW2/CW2Group/MapWebInterface:ConnectToBlobs" />
          </maps>
        </aCS>
        <aCS name="WebInterface:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/CW2/CW2Group/MapWebInterface:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="WebInterfaceInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/CW2/CW2Group/MapWebInterfaceInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:CloudletService:Endpoint1">
          <toPorts>
            <inPortMoniker name="/CW2/CW2Group/CloudletService/Endpoint1" />
          </toPorts>
        </lBChannel>
        <lBChannel name="LB:Engine:Endpoint1">
          <toPorts>
            <inPortMoniker name="/CW2/CW2Group/Engine/Endpoint1" />
          </toPorts>
        </lBChannel>
        <lBChannel name="LB:WebInterface:Endpoint1">
          <toPorts>
            <inPortMoniker name="/CW2/CW2Group/WebInterface/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapCertificate|WebInterface:Certificate1" kind="Identity">
          <certificate>
            <certificateMoniker name="/CW2/CW2Group/WebInterface/Certificate1" />
          </certificate>
        </map>
        <map name="MapCloudletService:Blobs" kind="Identity">
          <setting>
            <aCSMoniker name="/CW2/CW2Group/CloudletService/Blobs" />
          </setting>
        </map>
        <map name="MapCloudletService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/CW2/CW2Group/CloudletService/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapCloudletServiceInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/CW2/CW2Group/CloudletServiceInstances" />
          </setting>
        </map>
        <map name="MapEngine:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/CW2/CW2Group/Engine/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapEngineInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/CW2/CW2Group/EngineInstances" />
          </setting>
        </map>
        <map name="MapWebInterface:ConnectToBlobs" kind="Identity">
          <setting>
            <aCSMoniker name="/CW2/CW2Group/WebInterface/ConnectToBlobs" />
          </setting>
        </map>
        <map name="MapWebInterface:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/CW2/CW2Group/WebInterface/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapWebInterfaceInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/CW2/CW2Group/WebInterfaceInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="CloudletService" generation="1" functional="0" release="0" software="E:\CW\CW2\CW2\csx\Debug\roles\CloudletService" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="8082" />
            </componentports>
            <settings>
              <aCS name="Blobs" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;CloudletService&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;CloudletService&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;Engine&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;WebInterface&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/CW2/CW2Group/CloudletServiceInstances" />
            <sCSPolicyUpdateDomainMoniker name="/CW2/CW2Group/CloudletServiceUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/CW2/CW2Group/CloudletServiceFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="Engine" generation="1" functional="0" release="0" software="E:\CW\CW2\CW2\csx\Debug\roles\Engine" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="8080" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;Engine&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;CloudletService&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;Engine&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;WebInterface&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/CW2/CW2Group/EngineInstances" />
            <sCSPolicyUpdateDomainMoniker name="/CW2/CW2Group/EngineUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/CW2/CW2Group/EngineFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="WebInterface" generation="1" functional="0" release="0" software="E:\CW\CW2\CW2\csx\Debug\roles\WebInterface" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="https" portRanges="80">
                <certificate>
                  <certificateMoniker name="/CW2/CW2Group/WebInterface/Certificate1" />
                </certificate>
              </inPort>
            </componentports>
            <settings>
              <aCS name="ConnectToBlobs" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;WebInterface&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;CloudletService&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;Engine&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;WebInterface&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
            <storedcertificates>
              <storedCertificate name="Stored0Certificate1" certificateStore="My" certificateLocation="System">
                <certificate>
                  <certificateMoniker name="/CW2/CW2Group/WebInterface/Certificate1" />
                </certificate>
              </storedCertificate>
            </storedcertificates>
            <certificates>
              <certificate name="Certificate1" />
            </certificates>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/CW2/CW2Group/WebInterfaceInstances" />
            <sCSPolicyUpdateDomainMoniker name="/CW2/CW2Group/WebInterfaceUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/CW2/CW2Group/WebInterfaceFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="WebInterfaceUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyUpdateDomain name="EngineUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyUpdateDomain name="CloudletServiceUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="CloudletServiceFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="EngineFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="WebInterfaceFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="CloudletServiceInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="EngineInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="WebInterfaceInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="d643b57e-5100-4a09-b0d2-41cf440abf2f" ref="Microsoft.RedDog.Contract\ServiceContract\CW2Contract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="ba0e8ba2-825c-4ccb-b553-cf783c0a6021" ref="Microsoft.RedDog.Contract\Interface\CloudletService:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/CW2/CW2Group/CloudletService:Endpoint1" />
          </inPort>
        </interfaceReference>
        <interfaceReference Id="d859c436-e668-493d-9bbe-e53aef8353fe" ref="Microsoft.RedDog.Contract\Interface\Engine:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/CW2/CW2Group/Engine:Endpoint1" />
          </inPort>
        </interfaceReference>
        <interfaceReference Id="a11f5d84-6dc7-49f9-9676-061bb1c5d913" ref="Microsoft.RedDog.Contract\Interface\WebInterface:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/CW2/CW2Group/WebInterface:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>