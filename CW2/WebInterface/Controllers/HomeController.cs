﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.IO;
using WebInterface.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace WebInterface.Controllers
{
    public class HomeController : Controller
    {

        //Index - Upload
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult Delete(file model)
        {
            ServiceReference2.EngineClient client = new ServiceReference2.EngineClient();
            List<string> items = new List<string>(client.getFolderNames());

            if (ModelState.IsValid)
            {

                client.deleteFile(model.fileName);

            }
            else
            {
                return View("Index");
            }

            return View("Index", model);
        }



        //About View files

        [HttpGet]
        [Authorize]
        public ActionResult Names()
        {


            ServiceReference2.EngineClient client = new ServiceReference2.EngineClient();
            List<string> items = new List<string>(client.getFolderNames());


            return View(items);
        }
        [Authorize]
        public ActionResult Upload()
        {
            return View();
        }


        [HttpPost]
        [Authorize]
        public ActionResult Download(file model)
        {
            ServiceReference2.EngineClient client = new ServiceReference2.EngineClient();
            List<string> items = new List<string>(client.getFolderNames());


            var con = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

            //session value of the user
            string CU = User.Identity.Name;
            int T = 0;
            int N = 0;

            using (SqlConnection myConnection = new SqlConnection(con))
            {
                string oString = "Select NValue, TValue from AspNetUsers where UserName=@CU";
                SqlCommand oCmd = new SqlCommand(oString, myConnection);
                oCmd.Parameters.AddWithValue("@CU", CU);
                myConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        T = Convert.ToInt32(oReader["TValue"].ToString());
                        N = Convert.ToInt32(oReader["NValue"].ToString());
                    }

                    myConnection.Close();
                }
            }


            if (ModelState.IsValid)
            {
                byte[] rejoinedSecret = client.getSecret(model.fileName, N, T);
                System.IO.File.WriteAllBytes("E:\\CW\\CW2\\" + model.fileName, rejoinedSecret);

            }
            else
            {
                return View("Download");
            }

            return View("Download", model);
        }
        [Authorize]
        public ActionResult Download()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult Upload(HttpPostedFileBase file)
        {

            if (file == null)
                throw new ArgumentException("file");
            //create a client 
            ServiceReference2.EngineClient client = new ServiceReference2.EngineClient();
            //get some bytes: 
            string fileName = Path.GetFileName(file.FileName);
            // store the file inside ~/App_Data/uploads folder
            var filePath = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
            file.SaveAs(filePath);
            byte[] secret = System.IO.File.ReadAllBytes(filePath);
            var con = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

            //session 
            string CU = User.Identity.Name;
            int T = 0;
            int N = 0;

            using (SqlConnection myConnection = new SqlConnection(con))
            {
                string oString = "Select NValue, TValue from AspNetUsers where UserName=@CU";
                SqlCommand oCmd = new SqlCommand(oString, myConnection);
                oCmd.Parameters.AddWithValue("@CU", CU);
                myConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        N = Convert.ToInt32(oReader["NValue"].ToString());
                        T = Convert.ToInt32(oReader["TValue"].ToString());
                    }

                    myConnection.Close();
                }
            }

            //use the service and get a unique ID (string) back from the engine
            string fileId = client.storeSecret(secret, fileName, N, T);
            //the ViewBag object can be used to pass information to the View (look at the View/Home/About web page)
            ViewBag.Message = fileId;
            return View();
        }

    }
}