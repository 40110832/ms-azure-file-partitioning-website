﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebInterface.Models
{
    public class file
    {

        public HttpPostedFileBase File { get; set; }
        public string _T;
        public string _N;

        [Required(ErrorMessage = "This field can not be empty.")]
        public string fileName { get; set; }
        public string N
        {
            get { return _N; }
            set { _N = value; }
        }

        public string T
        {
            get { return _T; }
            set { _T = value; }
        }


    }

    //  public class TValue
    // {
    //    public string T { get; set; }
    //}

    //public class NValue
    // {
    //    public string N { get; set; }
    //}

}