﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Microsoft.Azure;
using Microsoft.WindowsAzure;
using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Cloudlet
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class CloudletService : ICloudlet
    {
        // Retrieve storage account from connection string.
        //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
        //CloudConfigurationManager.GetSetting("ConnectToBlobs"));
        //// Create the blob client.
        //CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
        // Retrieve a reference to a container.
        //CloudBlobContainer container = blobClient.GetContainerReference("icaracontainer");
        //// Create the container if it doesn't already exist.
        //container.CreateIfNotExists();



        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }


        public void upload(string path)
        {
            // Retrieve storage account from connection string. 
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("ConnectToBlobs"));
            // Create the blob client. 
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("icaracontainer");
            //// Create the container if it doesn't already exist.
            //container.CreateIfNotExists();
            // Retrieve reference to a blob named "myblob". 
            CloudBlockBlob blockBlob = container.GetBlockBlobReference("myblob");
            // Create or overwrite the "myblob" blob with contents from a local file. 
            using (var fileStream = System.IO.File.OpenRead(path)) {
                blockBlob.UploadFromStream(fileStream);
            }

        }

    }
}
