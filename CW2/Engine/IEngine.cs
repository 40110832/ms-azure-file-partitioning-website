﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Engine
{
    [ServiceContract]
    public interface IEngine
    {        
        [OperationContract]
        string storeSecret(byte[]secret,string fileName, int n, int t);

        [OperationContract]
        byte[] getSecret(string fileID, int n, int t);

        [OperationContract]
        List<string> getFolderNames();

        [OperationContract]
        void deleteFile(string fileName);
    }
}
