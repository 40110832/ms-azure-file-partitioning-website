﻿using sss;
using sss.config;
using sss.crypto.data;
using System;
using System.Collections.Generic;
using System.Web;

namespace Engine
{
    public class EngineService : IEngine
    {

        static CloudServiceRef.Service1Client cClient = new CloudServiceRef.Service1Client();

        public List<string> getFolderNames()
        {

            List<string> listOfFolders = new List<string>(cClient.containerContents());

            return listOfFolders;
        }


        public byte[] getSecret(string fileName, int n, int t)
        {

            //TODO (should connect to cloudlet to do this -- not implemented)
            string path = HttpRuntime.AppDomainAppPath + "/App_Data/";

            byte[] result = null;
            RandomSources r = RandomSources.SHA1;
            Encryptors e = Encryptors.ChaCha20;
            Algorithms a = Algorithms.CSS;

            //the individual shares
            List<Share> list = new List<Share>();
            try
            {
                Facade f = new Facade(n, t, r, e, a);
                //List<string> blobIds = blobs[fileName];
                List<string> blobIds = new List<string>(cClient.folderContents(fileName));

                foreach (string blobName in blobIds)
                {
                    byte[] data = cClient.download(blobName, fileName);
                    Share share = BaseShare.deserialize(data);
                    list.Add(share);
                }

                //recreate the original secret
                result = f.join(list.ToArray());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }

            return result;

        }

        public void deleteFile(string fileName)
        {
            cClient.delete(fileName);
        }

        public string storeSecret(byte[] secret, string fileName, int n, int t)
        {

            string path = HttpRuntime.AppDomainAppPath + "/App_Data/";

            // an unique id for the secret 
            Guid secretGuid = Guid.NewGuid();
            //string key = secretGuid.ToString();
            string key = fileName;

            //parameters for secret share algorithm (Facade below that use n and t passed into the method as parameters)
            RandomSources r = RandomSources.SHA1;
            Encryptors e = Encryptors.ChaCha20;
            Algorithms a = Algorithms.CSS;
            try
            {
                Facade f = new Facade(n, t, r, e, a);
                Share[] shares = f.split(secret);
                int j = 0;
                foreach (Share share in shares)
                {
                    j++;
                    //convert a share to a byte array
                    byte[] bytes = share.serialize();

                    //generate a unique ID
                    Guid guid = Guid.NewGuid();
                    // string blobName = guid.ToString();
                    string blobName = fileName + j;

                    cClient.upload(bytes, blobName, fileName);
                }
            }
            catch (Exception ex)
            {

            }

            return key;
        }
    }
}
