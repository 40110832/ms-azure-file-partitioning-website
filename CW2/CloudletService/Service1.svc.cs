﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;


namespace CloudletService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
        public void upload(byte[] bytes, string blobName, string fileName)
        {

            // Retrieve storage account from connection string. 
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("Blobs"));
            // Create the blob client. 
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

            var dir = container.GetDirectoryReference(fileName + "/");
            CloudBlockBlob blockBlob = dir.GetBlockBlobReference(blobName);

            using (var stream = new MemoryStream(bytes, writable: false))
            {

                blockBlob.UploadFromStream(stream);
            }

        }

        public byte[] download(string blobName, string fileName)
        {

            // Retrieve storage account from connection string. 
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("Blobs"));
            // Create the blob client. 
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
            // Retrieve reference to a blob named "myblob". 

            var dir = container.GetDirectoryReference(fileName + "/");


            CloudBlockBlob blockBlob = dir.GetBlockBlobReference(blobName);
            /// Create or overwrite the "myblob" blob with contents from a local file. 
            byte[] bytes = null;
            using (var stream = new MemoryStream())
            {
                blockBlob.DownloadToStream(stream);
                bytes = stream.ToArray();
            }
 
            return bytes;
        }
        public bool BlobExistsOnCloud()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("Blobs"));
            // Create the blob client. 
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
            // Retrieve reference to a blob named "myblob". 

            return blobClient.GetContainerReference("mycontainer").GetBlockBlobReference("TestDict/DictWithStats").Exists();
        }

        public List<string> containerContents()
        {
            // Retrieve storage account from connection string. 
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("Blobs"));
            // Create the blob client. 
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
            // Retrieve reference to a blob named "myblob". 

            List<string> listOfFileNames = new List<string>();

            string blobPrefix = null;
            bool useFlatBlobListing = false;
            var blobs = container.ListBlobs(blobPrefix, useFlatBlobListing, BlobListingDetails.None);
            var folders = blobs.Where(b => b as CloudBlobDirectory != null).ToList();
            foreach (var folder in folders)
            {
                //strfolder.Uri = folder.Uri.ToString().Remove(0, 61);
                listOfFileNames.Add((folder.Uri.ToString().Remove(0, 51)).TrimEnd('/'));
            }

            return listOfFileNames;

        }
        public List<string> folderContents(string fileName)
        {
            // Retrieve storage account from connection string. 
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("Blobs"));
            // Create the blob client. 
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
            // Retrieve reference to a blob named "myblob". 

            List<string> listOfFileNames = new List<string>();

            
            var blobs = container.ListBlobs(prefix:fileName, useFlatBlobListing: true);
  

            listOfFileNames = blobs.OfType<CloudBlockBlob>().Select(b => b.Name.Remove(0,b.Name.Length-fileName.Length-1)).ToList();
            

            return listOfFileNames;
        }


        public void delete(string fileName)
        {


            // Retrieve storage account from connection string. 
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("Blobs"));
            // Create the blob client. 
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

            foreach (IListBlobItem blob in container.GetDirectoryReference(fileName).ListBlobs(true))
            {
                if (blob.GetType() == typeof(CloudBlob) || blob.GetType().BaseType == typeof(CloudBlob))
                {
                    ((CloudBlob)blob).DeleteIfExists();
                }
            }
        }
    }
}
