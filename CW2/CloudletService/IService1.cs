﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;



namespace CloudletService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        [OperationContract]
        void upload(byte[] bytes, string blobName,string fileNme);

        [OperationContract]
        byte[] download(string blobName, string fileNme);

        [OperationContract]
        List<string> folderContents(string fileName);

        [OperationContract]
        bool BlobExistsOnCloud();

        [OperationContract]
        void delete(string fileName);

        [OperationContract]
        List<string> containerContents();

    }

    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello @@@";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
